﻿using System;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box("Electronics");
            Product television = new Product("Television", 999.99, 40.00);
            Product computer = new Product("Computer", 599.99, 23.00);
            Product speakers = new Product("Speakers", 99.99, 3.00);
            box.Add(television);
            box.Add(computer);
            box.Add(speakers);
            Console.WriteLine("Price: " + box.Price + " $");
            Console.WriteLine("Weight: " + box.Weight + " kg");
        }
    }
}
