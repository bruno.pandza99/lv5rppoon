﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD2
{
    class ShippingService
    {
        private double pricePerUnitOfMass;
        public ShippingService()
        {
            this.pricePerUnitOfMass = 0.5;
        }
        public ShippingService(double pricePerUnitOfMass)
        {
            this.pricePerUnitOfMass = pricePerUnitOfMass;
        }
        public double deliveryPrice(double weight)
        {
            double weightPrice = weight * pricePerUnitOfMass;
            return weightPrice;
        }


    }
}
